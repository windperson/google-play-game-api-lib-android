package com.capcom.netservice;

public enum GoogleApiConnectionState {
    CREATED, OPENING, OPENED, CLOSED
}
