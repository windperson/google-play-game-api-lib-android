package com.capcom.netservice;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class GoogleApiEventReceiver implements Observer {

    private final String logTag = "GoogleApiEventReceiver";

    private GoogleApiHandler _apiHandler;
    private GoogleApiConnectionState _curState;

    public GoogleApiEventReceiver(GoogleApiHandler observableApiHandler) {
        _apiHandler = observableApiHandler;
    }

    @Override
    public void update(Observable observable, Object data) {

        if (observable != _apiHandler) {
            return;
        }

        GoogleApiConnectionState state = (GoogleApiConnectionState) data;

        if (state != null) {

            if (state != _curState) {
                Log.d(logTag, "current=" + _curState + ", next=" + state);
                if (_onGoogleApiClientStateChangeListeners != null && !_onGoogleApiClientStateChangeListeners.isEmpty()) {
                    for (OnGoogleApiClientStateChangeListener listener : _onGoogleApiClientStateChangeListeners) {
                        listener.onGoogleApiClientStateChanged((GoogleApiHandler) observable, _curState, state);
                    }
                }
            }

            switch (state) {
                case CREATED: //User has successfully logined, needs to call GoogleApiClient.connect() again to ignite onConnected event.

                    break;

                case OPENING: //when GoogleApiClient auto open Google Account SignIn & Permission Auth UI

                    break;

                case OPENED:  //can safely send useful commands

                    break;

                case CLOSED: //initial state.

                    break;
            }
        }
        _curState = state;
    }

    /**
     * Anonymous Class Interface for GoogleApiClient change its state, then we do event notification.
     */
    public interface OnGoogleApiClientStateChangeListener {
        void onGoogleApiClientStateChanged(GoogleApiHandler googleApiHandler
                , GoogleApiConnectionState currentState
                , GoogleApiConnectionState newState);
    }

    private List<OnGoogleApiClientStateChangeListener> _onGoogleApiClientStateChangeListeners;

    public Boolean addOnGoogleApiClientStateChangeListener(OnGoogleApiClientStateChangeListener listener) {
        if (_onGoogleApiClientStateChangeListeners == null) {
            _onGoogleApiClientStateChangeListeners = new ArrayList<>();
        }
        return _onGoogleApiClientStateChangeListeners.add(listener);
    }

    public Boolean removeOnGoogleApiClientStateChangedListener(OnGoogleApiClientStateChangeListener listener) {

        return _onGoogleApiClientStateChangeListeners != null &&
                !_onGoogleApiClientStateChangeListeners.isEmpty() &&
                _onGoogleApiClientStateChangeListeners.remove(listener);
    }

    public Boolean isOnGoogleApiClientStateChangeListenerExist(OnGoogleApiClientStateChangeListener listener) {
        return _onGoogleApiClientStateChangeListeners != null &&
                !_onGoogleApiClientStateChangeListeners.isEmpty() &&
                _onGoogleApiClientStateChangeListeners.contains(listener);
    }

    public Boolean clearAllOnGoogleApiClientStateChangedListener() {
        if (_onGoogleApiClientStateChangeListeners != null) {
            _onGoogleApiClientStateChangeListeners.clear();
            _onGoogleApiClientStateChangeListeners = null;
            return true;
        }
        return false;
    }


}
