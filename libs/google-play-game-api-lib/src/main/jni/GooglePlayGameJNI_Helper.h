#ifndef TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYGAMEJNI_HELPER_H
#define TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYGAMEJNI_HELPER_H

#include "JNI_Helper.h"
#include "GooglePlayAchievement_Caller.h"
#include "GooglePlayLeaderBoard_Caller.h"

class GooglePlayGameJNI_Helper : public JNI_Helper{
private:
    GooglePlayGameJNI_Helper(JavaVM* vm, jint jni_version);
    jobject GoogleApiClient;
public:
    static GooglePlayGameJNI_Helper* getInstance(JavaVM* vm, jint jni_version, jobject googleApiClient);
    GooglePlayAchievement_Caller getGooglePlayAchievementCaller(std::string classFullName);
    GooglePlayLeaderBoard_Caller getGooglePlayLeaderBoardCaller(std::string classFullName);
    jobject getGoogleApiClient();
    ~GooglePlayGameJNI_Helper();
};


#endif //TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYGAMEJNI_HELPER_H
