/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gl2jni;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.capcom.netservice.GoogleApiHandler;
import com.capcom.netservice.GooglePlayGameServiceHelper;
import com.google.android.gms.common.api.GoogleApiClient;


public class GL2JNIActivity extends Activity {

    GL2JNIView mView;

    static {
        System.loadLibrary("google-play-game-api-lib");
    }

    private static String LOG_TAG = "Demo-Activity";

    // Client objects to interact with Google APIs
    private GoogleApiClient mGoogleApiClient;
    public GoogleApiClient getGoogleApiClient(){
        return mGoogleApiClient;
    }

    private GoogleApiHandler mApiHandler;


    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mView = new GL2JNIView(getApplication());
        setContentView(mView);
        initGoogleApiClient(this);
        initDemoBtnUI();
        initGooglePlayInvokeUI();
    }

    private void initGoogleApiClient(Activity activity) {
        mApiHandler = new GoogleApiHandler(activity);

        try {
            mGoogleApiClient = GooglePlayGameServiceHelper.getApiClient(mApiHandler);
            mApiHandler.setApiClient(mGoogleApiClient);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    private void initDemoBtnUI() {
        LinearLayout layout = new LinearLayout(this);
        layout.setGravity(Gravity.CENTER | Gravity.TOP);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutView = inflater.inflate(R.layout.test_btn, layout, false);
        layout.addView(layoutView);

        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        addContentView(layout, layoutParams);

        Button recordAchievementBtn = (Button) findViewById(R.id.call_achievement_record);
        recordAchievementBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG,"demo update achievement begin....");
                demoUpdateAchievement(getGoogleApiClient());
            }
        });

        Button recordLeaderBoardBtn = (Button) findViewById(R.id.call_leaderboard_record);
        recordLeaderBoardBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG,"demo record leader board begin....");
                demoUpdateLeaderBoard(getGoogleApiClient());
            }
        });

    }

    private native void demoUpdateAchievement(GoogleApiClient apiClient);
    private native void demoUpdateLeaderBoard(GoogleApiClient apiClient);

    private void initGooglePlayInvokeUI() {

        View.OnClickListener achievement_btn_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setClickable(false);

                Log.d(":achieveBtn", "open achievement UI...");
                GooglePlayGameServiceHelper.openAchievementUI(GL2JNIActivity.this, mGoogleApiClient, null, true, mApiHandler);

                view.setClickable(true);
            }
        };

        View.OnClickListener leaderBoard_btn_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setClickable(false);

                Log.d(":leaderBtn", "open leader board UI");
                GooglePlayGameServiceHelper.openLeaderBoardUI(GL2JNIActivity.this, mGoogleApiClient, null, true, mApiHandler);

                view.setClickable(true);
            }
        };

        GooglePlayGameServiceHelper.initGooglePlayInvokeUI(this,
                R.layout.google_play_service, R.id.achievement_btn_id, achievement_btn_listener,
                R.id.leaderBoard_btn_id, leaderBoard_btn_listener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //NOTE: iti is necessary to call this inorder to have some async behavior in GoogleApiClient functional.
        mApiHandler.checkOnActivityResultReturnStatus(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.onResume();
    }


}
