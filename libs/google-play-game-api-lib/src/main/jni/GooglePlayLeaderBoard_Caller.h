#ifndef TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYLEADERBOARD_CALLER_H
#define TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYLEADERBOARD_CALLER_H

#include "Api_Caller.h"

class GooglePlayLeaderBoard_Caller : public Api_Caller {
private:
    jobject _googlePlayGameServiceLeaderBoardJNI;
    jmethodID _javamethod_submitScore;
    jmethodID _javamethod_submitScore_with_tag;

public:
    GooglePlayLeaderBoard_Caller(JNIEnv *jniEnv,
                                 const std::string &classFullName,
                                 JNI_Helper *jniHelper,
                                 jobject googleApiClient);
    ~GooglePlayLeaderBoard_Caller();
    void submitScore(const char *leaderBoardId, long score);
    void submitScore(const char *leaderBoardId, long score, const char *scoreTag);
};


#endif //TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYLEADERBOARD_CALLER_H
