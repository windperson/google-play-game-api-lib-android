package com.capcom.netservice;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;


public class GoogleApiHandler extends Observable implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int RC_SIGN_IN = 59872;

    private WeakReference<Activity> _activityWeakReference;

    private final String logTag = "GoogleApiHandler";
    private GoogleApiClient _googleApiClient;

    private List<OnConnectedListener> _onConnectedListeners;
    private List<OnConnectionSuspendedListener> _onConnectionSuspendedListeners;
    private List<OnConnectFailedListener> _onConnectFailedListeners;
    private Player mPlayer;
    private GoogleApiConnectionState _currentState;

    public GoogleApiConnectionState getGoogleApiConnectionState() {
        return _currentState;
    }

    public GoogleApiHandler(Activity activity) {
        _activityWeakReference = new WeakReference<>(activity);
        _currentState = GoogleApiConnectionState.CLOSED;
    }

    public GoogleApiClient getApiClient() {
        return _googleApiClient;
    }

    private GoogleApiEventReceiver _eventReceiver;

    public void setGoogleApiEventReceiver(GoogleApiEventReceiver eventReceiver) {
        _eventReceiver = eventReceiver;
        addObserver(_eventReceiver);
    }

    public GoogleApiEventReceiver getGoogleApiEventReceiver() {
        return _eventReceiver;
    }

    public void setApiClient(GoogleApiClient apiClient) {
        _googleApiClient = apiClient;
        setGoogleApiEventReceiver(new GoogleApiEventReceiver(this));
    }

    public Activity getAttachedActivity() {
        return _activityWeakReference.get();
    }

    public Player getSignInPlayer() {
        return mPlayer;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlayer = Games.Players.getCurrentPlayer(_googleApiClient);
        if (_onConnectedListeners != null && !_onConnectedListeners.isEmpty()) {
            for (OnConnectedListener listener : _onConnectedListeners) {
                listener.onConnected(bundle);
            }
        }
        changeApiClientState(GoogleApiConnectionState.OPENED);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(logTag, "onConnectionSuspended(): i=" + i);

        // The connection to Google Play services was lost for some reason.
        // We call connect() to attempt to re-establish the connection or get a
        // ConnectionResult that we can attempt to resolve.
        changeApiClientState(GoogleApiConnectionState.CLOSED);
        _googleApiClient.connect();

        if (_onConnectionSuspendedListeners != null && !_onConnectionSuspendedListeners.isEmpty()) {
            for (OnConnectionSuspendedListener listener : _onConnectionSuspendedListeners) {
                listener.onConnectionSuspended(i);
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(logTag, "onConnectionFailed(): connectionResult=" + connectionResult.toString());
        //for handling user sign out error, it purpose is to let user to re-chose signIn account.
        if (connectionResult.hasResolution() && _currentState.equals(GoogleApiConnectionState.CLOSED)) {
            try {
                Log.d(logTag, "onConnectionFailed(): startResolutionForResult()...it will initiate Google Account login UI.");
                connectionResult.startResolutionForResult(getAttachedActivity(), RC_SIGN_IN);
                changeApiClientState(GoogleApiConnectionState.OPENING);
                return;
            } catch (IntentSender.SendIntentException e) {
                Log.d(logTag, "onConnectionFailed(): connect failed.");
                changeApiClientState(GoogleApiConnectionState.CLOSED);
            }
        } else {
            int errorCode = connectionResult.getErrorCode();
            Log.d(logTag, "onConnectionFailed(): error code=" + errorCode);

            GooglePlayGameServiceUtils.makeSimpleDialog(getAttachedActivity(), "error code=" + errorCode);
            changeApiClientState(GoogleApiConnectionState.CLOSED);
        }

        if (_onConnectFailedListeners != null && !_onConnectFailedListeners.isEmpty()) {
            for (OnConnectFailedListener listener : _onConnectFailedListeners) {
                listener.onConnectionFailed(connectionResult);
            }
        }
    }

    public void checkOnActivityResultReturnStatus(int requestCode, int resultCode, Intent data) {

        if (GoogleApiHandler.RC_SIGN_IN == requestCode) {

            if (resultCode == Activity.RESULT_OK) {
                // If the error resolution was successful we should continue
                // processing errors.
                changeApiClientState(GoogleApiConnectionState.CREATED);
            } else {
                // If the error resolution was not successful or the user canceled,
                // we should stop processing errors.
                changeApiClientState(GoogleApiConnectionState.CLOSED);
                GooglePlayGameServiceUtils.showActivityResultError(
                        getAttachedActivity(),requestCode,resultCode,R.string.netservicelib_google_play_signin_error);
            }
        }

        //user click sign out button in Google's achievement / Leader Board UI
        if (GooglePlayGameServiceHelper.OPEN_GOOGLE_PROVIDED_UI == requestCode
                && GooglePlayGameServiceHelper.USER_SIGNOUT_BY_GOOGLE_UI == resultCode) {
            closeApiClientConnection();
            getGoogleApiEventReceiver().clearAllOnGoogleApiClientStateChangedListener();
            clearAllListeners();
        }

    }

    private void changeApiClientState(GoogleApiConnectionState state) {
        _currentState = state;
        setChanged();
        notifyObservers(state);
    }

    protected void closeApiClientConnection() {
        _googleApiClient.disconnect();
        changeApiClientState(GoogleApiConnectionState.CLOSED);
    }


    /**
     * Anonymous Class Interface for GoogleApiClient "Connected" event listener
     */
    public interface OnConnectedListener {
        void onConnected(@Nullable Bundle bundle);
    }

    /**
     * Anonymous Class Interface for GoogleApiClient "ConnectionSuspended" event listener
     */
    public interface OnConnectionSuspendedListener {
        void onConnectionSuspended(int i);
    }

    /**
     * Anonymous Class Interface for GoogleApiClient "ConnectFailed" event listener
     */
    public interface OnConnectFailedListener {
        void onConnectionFailed(@NonNull ConnectionResult connectionResult);
    }

    public Boolean addOnConnectedListener(OnConnectedListener listener) {
        if (_onConnectedListeners == null) {
            _onConnectedListeners = new ArrayList<>();
        }
        return _onConnectedListeners.add(listener);
    }

    public Boolean removeOnConnectedListener(OnConnectedListener listener) {
        return _onConnectedListeners != null &&
                !_onConnectedListeners.isEmpty() &&
                _onConnectedListeners.remove(listener);
    }

    public Boolean isOnConnectedListenerExist(OnConnectedListener listener) {
        return _onConnectedListeners != null &&
                !_onConnectedListeners.isEmpty() &&
                _onConnectedListeners.contains(listener);
    }

    private Boolean clearOnConnectedListeners() {
        if (_onConnectedListeners != null) {
            _onConnectedListeners.clear();
            _onConnectedListeners = null;
            return true;
        }
        return false;
    }


    public Boolean addOnConnectionSuspendedListener(OnConnectionSuspendedListener listener) {
        if (_onConnectionSuspendedListeners == null) {
            _onConnectionSuspendedListeners = new ArrayList<>();
        }
        return _onConnectionSuspendedListeners.add(listener);
    }

    public Boolean removeOnConnectionSuspendedListener(OnConnectionSuspendedListener listener) {
        return _onConnectionSuspendedListeners != null &&
                !_onConnectionSuspendedListeners.isEmpty() &&
                _onConnectionSuspendedListeners.remove(listener);
    }

    public Boolean isOnConnectionSuspendedListenerExist(OnConnectionSuspendedListener listener) {
        return _onConnectionSuspendedListeners != null &&
                !_onConnectionSuspendedListeners.isEmpty() &&
                _onConnectionSuspendedListeners.contains(listener);
    }

    private Boolean clearOnConnectionSuspendedListeners() {
        if (_onConnectionSuspendedListeners != null) {
            _onConnectionSuspendedListeners.clear();
            _onConnectionSuspendedListeners = null;
            return true;
        }
        return false;
    }

    public Boolean addOnConnectFailedListener(OnConnectFailedListener listener) {
        if (_onConnectFailedListeners == null) {
            _onConnectFailedListeners = new ArrayList<>();
        }
        return _onConnectFailedListeners.add(listener);
    }

    public Boolean removeOnConnectFailedListener(OnConnectFailedListener listener) {
        return _onConnectFailedListeners != null &&
                !_onConnectFailedListeners.isEmpty() &&
                _onConnectFailedListeners.remove(listener);
    }

    public Boolean isOnConnectFailedListenerExist(OnConnectFailedListener listener) {
        return _onConnectFailedListeners != null &&
                !_onConnectFailedListeners.isEmpty() &&
                _onConnectFailedListeners.contains(listener);
    }

    private Boolean clearOnConnectFailedListeners() {
        if (_onConnectFailedListeners != null) {
            _onConnectFailedListeners.clear();
            _onConnectFailedListeners = null;
            return true;
        }
        return false;
    }

    private void clearAllListeners() {
        clearOnConnectedListeners();
        clearOnConnectionSuspendedListeners();
        clearOnConnectFailedListeners();
    }
}
