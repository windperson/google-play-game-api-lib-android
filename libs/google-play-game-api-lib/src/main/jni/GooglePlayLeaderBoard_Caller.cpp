//
// Created by isak on 10/28/2016.
//

#include "GooglePlayLeaderBoard_Caller.h"

GooglePlayLeaderBoard_Caller::GooglePlayLeaderBoard_Caller(JNIEnv *jniEnv,
                                                           const std::string &classFullName,
                                                           JNI_Helper *jniHelper,
                                                           jobject googleApiClient) :
        Api_Caller(jniEnv, classFullName, jniHelper) {
    jmethodID javaClassConstructor = this->_env->GetMethodID(this->_calleeClass, "<init>",
                                                             "(Lcom/google/android/gms/common/api/GoogleApiClient;)V");
    jobject instance = this->_env->NewObject(this->_calleeClass, javaClassConstructor,
                                             googleApiClient);
    this->_googlePlayGameServiceLeaderBoardJNI = this->_env->NewGlobalRef(instance);

    this->_javamethod_submitScore = this->_env->GetMethodID(this->_calleeClass, "submitScore",
                                                            "(Ljava/lang/String;J)V");
    this->_javamethod_submitScore_with_tag = this->_env->GetMethodID(this->_calleeClass,
                                                                     "submitScore",
                                                                     "(Ljava/lang/String;JLjava/lang/String;)V");
}

void GooglePlayLeaderBoard_Caller::submitScore(const char *leaderBoardId, long score) {
    jstring input_leaderBoardId = this->_env->NewStringUTF(leaderBoardId);
    jlong input_score = score;
    this->_env->CallVoidMethod(this->_googlePlayGameServiceLeaderBoardJNI,
                               this->_javamethod_submitScore, input_leaderBoardId, input_score);
    if (this->_env->ExceptionCheck()) {
        this->_env->ExceptionDescribe();
        this->_env->ExceptionClear();
    }
    return;
}

void GooglePlayLeaderBoard_Caller::submitScore(const char *leaderBoardId, long score,
                                               const char *scoreTag) {
    jstring input_leaderBoardId = this->_env->NewStringUTF(leaderBoardId);
    jlong input_score = score;
    jstring input_scoreTag = this->_env->NewStringUTF(scoreTag);
    this->_env->CallVoidMethod(this->_googlePlayGameServiceLeaderBoardJNI,
                               this->_javamethod_submitScore_with_tag, input_leaderBoardId,
                               input_score, input_scoreTag);
    if (this->_env->ExceptionCheck()) {
        this->_env->ExceptionDescribe();
        this->_env->ExceptionClear();
    }
    return;
}

GooglePlayLeaderBoard_Caller::~GooglePlayLeaderBoard_Caller() {
    if (this->_googlePlayGameServiceLeaderBoardJNI != NULL) {
        this->_env->DeleteGlobalRef(this->_googlePlayGameServiceLeaderBoardJNI);
        this->_googlePlayGameServiceLeaderBoardJNI = NULL;
    }

}









