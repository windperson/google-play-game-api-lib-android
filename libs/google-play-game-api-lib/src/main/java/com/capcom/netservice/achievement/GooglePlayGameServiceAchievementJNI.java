package com.capcom.netservice.achievement;



import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;

public class GooglePlayGameServiceAchievementJNI {

    private GoogleApiClient _client;

    public GooglePlayGameServiceAchievementJNI(GoogleApiClient apiClient){
        _client = apiClient;
    }

    public void unlock(String achievementId) throws Exception {
        if(_client!=null){
            _client.connect();
            Games.Achievements.unlock(_client, achievementId);
        }
        else{
            throw new Exception("Google Api Client is null");
        }
    }

    public void increment(String achievementId, int steps) throws Exception {
        if(_client!=null) {
            _client.connect();
            Games.Achievements.increment(_client, achievementId, steps);
        }
        else{
            throw new Exception("Google Api Client is null");
        }
    }

    public void setSteps(String achievementId, int numSteps) throws Exception {
        if(_client!=null) {
            _client.connect();
            Games.Achievements.setSteps(_client, achievementId, numSteps);
        }
        else{
            throw new Exception("Google Api Client is null");
        }
    }

    public void reveal(String achievementId) throws Exception {
        if(_client!=null) {
            _client.connect();
            Games.Achievements.reveal(_client, achievementId);
        }
        else{
            throw new Exception("Google Api Client is null");
        }
    }

}
