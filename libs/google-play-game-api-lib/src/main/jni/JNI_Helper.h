#ifndef TEST_GRADLE_EXPERIMENTAL_PLUGIN_JNI_HELPER_H
#define TEST_GRADLE_EXPERIMENTAL_PLUGIN_JNI_HELPER_H


#include <jni.h>
#include <string>
#include "Api_Caller.h"

struct ThreadedClassLoader{
    jobject ClassLoader;
    jmethodID FindClassMethod;

};

class Api_Caller;


class JNI_Helper {
private:
    JavaVM *_vm;
    jint _jni_ver;
protected:
    static JNI_Helper *singleton;
    JNI_Helper(JavaVM* vm, jint jni_version);

public:
    static jint OnLoadJNIVersionCheck(JavaVM *vm);
    static JNI_Helper* getInstance(JavaVM* vm, jint jni_version);
    static jint determineJNI_Env_Valid(JavaVM *vm, JNIEnv *env, jint jni_version);
    static bool cleanupJNIEnv(JNIEnv *jniEnv, bool isDetached);
    Api_Caller getJavaCaller(std::string classFullName);
    virtual ~JNI_Helper();

    ThreadedClassLoader threadedClassLoader;
    void setupThreadedClassLoader(JNIEnv *env, const char *classFullName);
    JNIEnv *getJNIEnv(bool* isDetached);
    jclass findClass(const char* name, JNIEnv *env);
};



#endif //TEST_GRADLE_EXPERIMENTAL_PLUGIN_JNI_HELPER_H
