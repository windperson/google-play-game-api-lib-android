package com.capcom.netservice;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;

/**
 * Helper class for something relate to Google Play Game Service.
 */

public class GooglePlayGameServiceHelper {

    // request codes we use when invoking an external activity
    public static int OPEN_GOOGLE_PROVIDED_UI = 5001;
    /**
     * MAGIC code, DON'T know if google will change its signout menu result code in the future.
     */
    public static int USER_SIGNOUT_BY_GOOGLE_UI = 10001;


    public static void initGooglePlayInvokeUI(Activity activity,
                                              final int xml_res_id,
                                              final int achievement_btn_id, View.OnClickListener achievement_btn_listener,
                                              final int leaderBoard_btn_id, View.OnClickListener leaderBoard_btn_listener) {

        if(activity == null || xml_res_id == 0) {return;}

        RelativeLayout layout = new RelativeLayout(activity);

        layout.setGravity(Gravity.CENTER | Gravity.END);

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutView = inflater.inflate(xml_res_id, layout, false);
        layout.addView(layoutView);

        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        activity.addContentView(layout, layoutParams);

        if(achievement_btn_id != 0 && achievement_btn_listener != null){
            ImageButton achieveBtn;
            achieveBtn = (ImageButton) activity.findViewById(achievement_btn_id);
            achieveBtn.setOnClickListener(achievement_btn_listener);
        }

        if(leaderBoard_btn_id != 0 && leaderBoard_btn_listener != null){
            ImageButton leaderBtn;

            leaderBtn = (ImageButton) activity.findViewById(leaderBoard_btn_id);
            leaderBtn.setOnClickListener(leaderBoard_btn_listener);
        }
    }

    @NonNull
    public static GoogleApiClient getApiClient(@NonNull GoogleApiHandler apiHandler) {

        return new GoogleApiClient.Builder(apiHandler.getAttachedActivity())
                .addConnectionCallbacks(apiHandler)
                .addOnConnectionFailedListener(apiHandler)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();
    }

    private static GoogleApiEventReceiver.OnGoogleApiClientStateChangeListener onGoogleApiClientStateChangeListener;

    private static void registerOnGoogleApiClientConnected(final GoogleApiHandler apiHandler){
        if (onGoogleApiClientStateChangeListener == null) {
            onGoogleApiClientStateChangeListener =
                    new GoogleApiEventReceiver.OnGoogleApiClientStateChangeListener() {
                        @Override
                        public void onGoogleApiClientStateChanged(GoogleApiHandler googleApiHandler, GoogleApiConnectionState currentState, GoogleApiConnectionState newState) {
                            //NOTE: We need to invoke GoogleApi.Client.connect() again inorder to let "onConnected" event fire!
                            if (currentState == GoogleApiConnectionState.OPENING && newState == GoogleApiConnectionState.CREATED) {
                                apiHandler.getApiClient().connect();
                            }
                        }
                    };
        }
    }

    private static GoogleApiHandler.OnConnectedListener onConnectOpenAchievementListener;

    public static void openAchievementUI(Activity activity, final GoogleApiClient apiClient,
                                         String error_prompt,
                                         Boolean autoLogin, final GoogleApiHandler apiHandler) {
        if (isSignIn(apiClient, apiHandler)) {
            startGoogleAchievementActivity(activity, apiClient);
        } else {
            if (error_prompt == null || error_prompt.length() == 0) {
                error_prompt = activity.getString(R.string.netservicelib_achievements_not_available);
            }

            if (onConnectOpenAchievementListener == null) {
                onConnectOpenAchievementListener =
                        new GoogleApiHandler.OnConnectedListener() {

                            @Override
                            public void onConnected(@Nullable Bundle bundle) {
                                Activity attachedActivity = apiHandler.getAttachedActivity();
                                startGoogleAchievementActivity(attachedActivity, apiHandler.getApiClient());
                            }
                        };
            }

            if (autoLogin) {

                registerOnGoogleApiClientConnected(apiHandler);

                DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!apiHandler.getGoogleApiEventReceiver()
                                .isOnGoogleApiClientStateChangeListenerExist(onGoogleApiClientStateChangeListener)) {
                            apiHandler.getGoogleApiEventReceiver().addOnGoogleApiClientStateChangeListener(onGoogleApiClientStateChangeListener);
                        }

                        if (!apiHandler.isOnConnectedListenerExist(onConnectOpenAchievementListener)) {
                            apiHandler.addOnConnectedListener(onConnectOpenAchievementListener);
                        }
                        apiClient.connect();
                    }
                };
                GooglePlayGameServiceUtils.makeYesNoDialog(activity, error_prompt, yesListener, null).show();
            } else {
                GooglePlayGameServiceUtils.makeSimpleDialog(activity, error_prompt).show();
            }
        }
    }

    private static void startGoogleAchievementActivity(Activity activity, GoogleApiClient apiClient) {
        apiClient.connect();
        activity.startActivityForResult(Games.Achievements.getAchievementsIntent(apiClient), OPEN_GOOGLE_PROVIDED_UI);
    }

    private static GoogleApiHandler.OnConnectedListener onConnectOpenLeaderBoardListener;

    public static void openLeaderBoardUI(Activity activity, final GoogleApiClient apiClient,
                                         String error_prompt,
                                         Boolean autoLogin, final GoogleApiHandler apiHandler) {
        if (isSignIn(apiClient, apiHandler)) {
            startGoogleLeaderBoardActivity(activity, apiClient);
        } else {
            if (error_prompt == null || error_prompt.length() == 0) {
                error_prompt = activity.getString(R.string.netservicelib_leaderboards_not_available);
            }

            if (onConnectOpenLeaderBoardListener == null) {
                onConnectOpenLeaderBoardListener =
                        new GoogleApiHandler.OnConnectedListener() {

                            @Override
                            public void onConnected(@Nullable Bundle bundle) {
                                Activity attachedActivity = apiHandler.getAttachedActivity();
                                startGoogleLeaderBoardActivity(attachedActivity, apiHandler.getApiClient());
                            }
                        };
            }

            if (autoLogin) {

                registerOnGoogleApiClientConnected(apiHandler);

                DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (!apiHandler.getGoogleApiEventReceiver()
                                .isOnGoogleApiClientStateChangeListenerExist(onGoogleApiClientStateChangeListener)) {
                            apiHandler.getGoogleApiEventReceiver().addOnGoogleApiClientStateChangeListener(onGoogleApiClientStateChangeListener);
                        }

                        if (!apiHandler.isOnConnectedListenerExist(onConnectOpenLeaderBoardListener)) {
                            apiHandler.addOnConnectedListener(onConnectOpenLeaderBoardListener);
                        }
                        apiClient.connect();
                    }
                };
                GooglePlayGameServiceUtils.makeYesNoDialog(activity, error_prompt, yesListener, null).show();
            } else {
                GooglePlayGameServiceUtils.makeSimpleDialog(activity, error_prompt).show();
            }
        }
    }

    private static void startGoogleLeaderBoardActivity(Activity activity, GoogleApiClient apiClient) {
        apiClient.connect();
        activity.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(apiClient), OPEN_GOOGLE_PROVIDED_UI);
    }


    private static boolean isSignIn(GoogleApiClient apiClient, GoogleApiHandler apiHandler) {

        Boolean isConnected = (apiClient != null && apiClient.isConnected());
        Boolean isSignIned = (apiHandler != null && apiHandler.getGoogleApiConnectionState().equals(GoogleApiConnectionState.OPENED));

        return isConnected && isSignIned;
    }

    protected static void signOutAsync(GoogleApiClient apiClient, GoogleApiHandler apiHandler) {

        final GoogleApiHandler googleApiHandler = apiHandler;
        if (!apiClient.isConnected() && googleApiHandler.getGoogleApiConnectionState().equals(GoogleApiConnectionState.CLOSED)) {
            return;
        }
        PendingResult<Status> pendingResult = Games.signOut(apiClient);
        pendingResult.setResultCallback(new ResultCallback<Result>() {
            @Override
            public void onResult(@NonNull Result result) {
                googleApiHandler.closeApiClientConnection();
            }
        });
    }

}
