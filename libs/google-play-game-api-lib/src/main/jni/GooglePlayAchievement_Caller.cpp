#include "GooglePlayAchievement_Caller.h"

GooglePlayAchievement_Caller::GooglePlayAchievement_Caller(JNIEnv *jniEnv,
                                                           const std::string &classFullName,
                                                           JNI_Helper *jniHelper,
                                                           jobject googleApiClient) :
        Api_Caller(jniEnv, classFullName, jniHelper) {

    jmethodID javaClassConstructor = this->_env->GetMethodID(this->_calleeClass, "<init>",
                                                             "(Lcom/google/android/gms/common/api/GoogleApiClient;)V");
    jobject instance = this->_env->NewObject(this->_calleeClass, javaClassConstructor,
                                             googleApiClient);
    this->_googlePlayGameServiceAchievementJNI = this->_env->NewGlobalRef(instance);

    this->_javamethod_unlock = this->_env->GetMethodID(this->_calleeClass, "unlock",
                                                       "(Ljava/lang/String;)V");
    this->_javamethod_increment = this->_env->GetMethodID(this->_calleeClass, "increment",
                                                          "(Ljava/lang/String;I)V");
    this->_javamethod_setSteps = this->_env->GetMethodID(this->_calleeClass, "setSteps",
                                                         "(Ljava/lang/String;I)V");
    this->_javamethod_reveal = this->_env->GetMethodID(this->_calleeClass, "reveal",
                                                       "(Ljava/lang/String;)V");

}

void GooglePlayAchievement_Caller::unlock(const char *achievementId) {
    jstring input_achievementId = this->_env->NewStringUTF(achievementId);
    this->_env->CallVoidMethod(this->_googlePlayGameServiceAchievementJNI, this->_javamethod_unlock,
                               input_achievementId);
    if (this->_env->ExceptionCheck()) {
        this->_env->ExceptionDescribe();
        this->_env->ExceptionClear();
    }
    return;
}

void GooglePlayAchievement_Caller::increment(const char *achievementId, int steps) {
    jstring input_achievementId = this->_env->NewStringUTF(achievementId);
    jint input_steps = steps;
    this->_env->CallVoidMethod(this->_googlePlayGameServiceAchievementJNI,
                               this->_javamethod_increment, input_achievementId, input_steps);
    if (this->_env->ExceptionCheck()) {
        this->_env->ExceptionDescribe();
        this->_env->ExceptionClear();
    }
    return;
}

void GooglePlayAchievement_Caller::setSteps(const char *achievementId, int numSteps) {
    jstring input_achievementId = this->_env->NewStringUTF(achievementId);
    jint input_numSteps = numSteps;
    this->_env->CallVoidMethod(this->_googlePlayGameServiceAchievementJNI,
                               this->_javamethod_setSteps, input_achievementId, input_numSteps);
    if (this->_env->ExceptionCheck()) {
        this->_env->ExceptionDescribe();
        this->_env->ExceptionClear();
    }
    return;
}

void GooglePlayAchievement_Caller::reveal(const char *achievementId) {
    jstring input_achievementId = this->_env->NewStringUTF(achievementId);
    this->_env->CallVoidMethod(this->_googlePlayGameServiceAchievementJNI, this->_javamethod_reveal,
                               input_achievementId);
    if (this->_env->ExceptionCheck()) {
        this->_env->ExceptionDescribe();
        this->_env->ExceptionClear();
    }
    return;
}

GooglePlayAchievement_Caller::~GooglePlayAchievement_Caller() {
    if (this->_googlePlayGameServiceAchievementJNI != NULL) {
        this->_env->DeleteGlobalRef(this->_googlePlayGameServiceAchievementJNI);
        this->_googlePlayGameServiceAchievementJNI = NULL;
    }

}









