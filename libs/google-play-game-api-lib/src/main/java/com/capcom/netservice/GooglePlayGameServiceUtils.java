package com.capcom.netservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.GamesActivityResultCodes;

/**
 * Utiltiy functions to help handling Google Play Game Service connection
 */
public class GooglePlayGameServiceUtils {

    /**
     * Show a {@link android.app.Dialog} with the correct message for a connection error.
     *
     * @param activity         the Activity in which the Dialog should be displayed.
     * @param requestCode      the request code from onActivityResult.
     * @param actResp          the response code from onActivityResult.
     * @param errorDescription the resource id of a String for a generic error message.
     */
    public static void showActivityResultError(Activity activity, int requestCode, int actResp, int errorDescription) {
        if (activity == null) {
            Log.e("BaseGameUtils", "*** No Activity. Can't show failure dialog!");
            return;
        }
        Dialog errorDialog;

        switch (actResp) {
            case GamesActivityResultCodes.RESULT_APP_MISCONFIGURED:
                errorDialog = makeSimpleDialog(activity,
                        activity.getString(R.string.netservicelib_app_misconfigured));
                break;
            case GamesActivityResultCodes.RESULT_SIGN_IN_FAILED:
                errorDialog = makeSimpleDialog(activity,
                        activity.getString(R.string.netservicelib_sign_in_failed));
                break;
            case GamesActivityResultCodes.RESULT_LICENSE_FAILED:
                errorDialog = makeSimpleDialog(activity,
                        activity.getString(R.string.netservicelib_license_failed));
                break;
            default:
                // No meaningful Activity response code, so generate default Google
                // Play services dialog
                final int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
                errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
                        activity, requestCode, null);
                if (errorDialog == null) {
                    // get fallback dialog
                    Log.e("BaseGamesUtils",
                            "No standard error dialog available. Making fallback dialog.");
                    errorDialog = makeSimpleDialog(activity, activity.getString(errorDescription));
                }
        }

        errorDialog.show();
    }

    /**
     * Create a simple {@link Dialog} with an 'OK' button and a message.
     *
     * @param activity the Activity in which the Dialog should be displayed.
     * @param text     the message to display on the Dialog.
     * @return an instance of {@link AlertDialog}
     */
    public static Dialog makeSimpleDialog(Activity activity, String text) {
        AlertDialog.Builder builder = getBuilder(activity);
        return builder.setMessage(text).setNeutralButton(android.R.string.ok, null).create();
    }

    private static AlertDialog.Builder getBuilder(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            return new AlertDialog.Builder(activity, android.R.style.Theme_DeviceDefault_Dialog_Alert);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return new AlertDialog.Builder(activity, android.R.style.Theme_DeviceDefault_Dialog);
        } else {
            return new AlertDialog.Builder(activity);
        }
    }

    public static Dialog makeYesNoDialog(Activity activity, String text,
                                         DialogInterface.OnClickListener yesListener,
                                         DialogInterface.OnClickListener noListener) {
        AlertDialog.Builder builder = getBuilder(activity);
        return builder.setMessage(text)
                .setPositiveButton(android.R.string.yes, yesListener)
                .setNeutralButton(android.R.string.no, noListener).create();
    }

}
