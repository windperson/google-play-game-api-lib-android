//
// Created by isak on 10/28/2016.
//

#ifndef TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYACHIEVEMENT_CALLER_H
#define TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYACHIEVEMENT_CALLER_H


#include "Api_Caller.h"

class GooglePlayAchievement_Caller : public Api_Caller {
private:
    jobject _googlePlayGameServiceAchievementJNI;
    jmethodID _javamethod_unlock;
    jmethodID _javamethod_increment;
    jmethodID _javamethod_setSteps;
    jmethodID _javamethod_reveal;

public:
    GooglePlayAchievement_Caller(JNIEnv *jniEnv,
                                 const std::string &classFullName,
                                 JNI_Helper *jniHelper,
                                 jobject googleApiClient);
    ~GooglePlayAchievement_Caller();
    void unlock(const char *achievementId);
    void increment(const char *achievementId, int steps);
    void setSteps(const char *achievementId, int numSteps);
    void reveal(const char *achievementId);

};


#endif //TEST_GRADLE_EXPERIMENTAL_PLUGIN_GOOGLEPLAYACHIEVEMENT_CALLER_H
