package com.capcom.netservice.leaderBoard;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;


public class GooglePlayGameServiceLeaderBoardJNI {
    private GoogleApiClient _client;

    public GooglePlayGameServiceLeaderBoardJNI(GoogleApiClient apiClient){
        _client = apiClient;
    }

    public void submitScore(String leaderBoardId, long score) throws Exception {
        if(_client!=null) {
            _client.connect();
            Games.Leaderboards.submitScore(_client, leaderBoardId, score);
        }
        else{
            throw new Exception("Google Api Client is null");
        }
    }

    public void submitScore(String leaderBoardId, long score, String scoreTag) throws Exception {
        if(_client!=null) {
            _client.connect();
            Games.Leaderboards.submitScore(_client, leaderBoardId, score, scoreTag);
        }
        else{
            throw new Exception("Google Api Client is null");
        }
    }
}
