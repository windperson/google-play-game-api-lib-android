#include <jni.h>
#include <pthread.h>
#include <android/log.h>
#include <GooglePlayGameJNI_Helper.h>


#define LOG_TAG "demo_google_play_game_api_jni"
#define LOGINFO(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGERROR(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)


static JavaVM *vm_ref;
static jint RUNTIME_JNI_VERSION;

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    vm_ref = vm;

    RUNTIME_JNI_VERSION = GooglePlayGameJNI_Helper::OnLoadJNIVersionCheck(vm);

    return RUNTIME_JNI_VERSION;
}

struct ThreadWorkerArgs {
    GooglePlayGameJNI_Helper *googlePlayGameJNIHelper;
};


void *call_achievement_in_pthread(void *args) {
    ThreadWorkerArgs *threadWorkerArgs = (ThreadWorkerArgs *) args;
    GooglePlayGameJNI_Helper *helper = threadWorkerArgs->googlePlayGameJNIHelper;
    GooglePlayAchievement_Caller caller = helper->getGooglePlayAchievementCaller(
            "com/capcom/netservice/achievement/GooglePlayGameServiceAchievementJNI");

    LOGINFO("record the increment achievement");
    caller.increment("CgkI1-zo584MEAIQBA", 1);

    LOGINFO("unlock an achievement");
    caller.unlock("CgkI1-zo584MEAIQBw");

    return 0;
}

void *call_leaderboard_in_pthread(void *args) {
    ThreadWorkerArgs *threadWorkerArgs = (ThreadWorkerArgs *) args;
    GooglePlayGameJNI_Helper *helper = threadWorkerArgs->googlePlayGameJNIHelper;
    GooglePlayLeaderBoard_Caller caller = helper->getGooglePlayLeaderBoardCaller(
            "com/capcom/netservice/leaderBoard/GooglePlayGameServiceLeaderBoardJNI");

    LOGINFO("submit leader board score");
    caller.submitScore("CgkI1-zo584MEAIQCQ", 123);

    return 0;
}


extern "C" {
JNIEXPORT void JNICALL
Java_com_android_gl2jni_GL2JNIActivity_demoUpdateAchievement(JNIEnv *env, jobject instance,
                                                             jobject apiClient) {

    GooglePlayGameJNI_Helper *helper = GooglePlayGameJNI_Helper::getInstance(vm_ref,
                                                                             RUNTIME_JNI_VERSION,
                                                                             apiClient);
    helper->setupThreadedClassLoader(helper->getJNIEnv(NULL),
                                     "com/capcom/netservice/achievement/GooglePlayGameServiceAchievementJNI");

    ThreadWorkerArgs *threadWorkerArgs = new ThreadWorkerArgs();
    threadWorkerArgs->googlePlayGameJNIHelper = helper;

    pthread_t thread;
    int result = pthread_create(&thread, NULL, call_achievement_in_pthread,
                                (void *) threadWorkerArgs);

    if (0 != result) {
        jclass exClass = env->FindClass("java/lang/RuntimeException");
        env->ThrowNew(exClass, "Unable to create thread.");
    }
}

JNIEXPORT void JNICALL
Java_com_android_gl2jni_GL2JNIActivity_demoUpdateLeaderBoard(JNIEnv *env, jobject instance,
                                                             jobject apiClient) {

    GooglePlayGameJNI_Helper *helper = GooglePlayGameJNI_Helper::getInstance(vm_ref,
                                                                             RUNTIME_JNI_VERSION,
                                                                             apiClient);
    helper->setupThreadedClassLoader(helper->getJNIEnv(NULL),
                                     "com/capcom/netservice/leaderBoard/GooglePlayGameServiceLeaderBoardJNI");

    ThreadWorkerArgs *threadWorkerArgs = new ThreadWorkerArgs();
    threadWorkerArgs->googlePlayGameJNIHelper = helper;

    pthread_t thread;
    int result = pthread_create(&thread, NULL, call_leaderboard_in_pthread,
                                (void *) threadWorkerArgs);

    if (0 != result) {
        jclass exClass = env->FindClass("java/lang/RuntimeException");
        env->ThrowNew(exClass, "Unable to create thread.");
    }

}

};
