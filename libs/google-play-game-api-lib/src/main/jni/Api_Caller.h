#ifndef TEST_GRADLE_EXPERIMENTAL_PLUGIN_API_CALLER_H
#define TEST_GRADLE_EXPERIMENTAL_PLUGIN_API_CALLER_H


#include <jni.h>
#include <string>
#include "JNI_Helper.h"


class JNI_Helper;


class Api_Caller {
private:
    std::string _classPath;
    jclass initJavaClassRef();

protected:
    JNIEnv *_env;
    jclass _calleeClass;

public:
    Api_Caller(JNIEnv* jniEnv, std::string classFullName, JNI_Helper *jniHelper);
    virtual ~Api_Caller();
    bool Detached;
    JNI_Helper *JNIHelper;
    void invokeVoidJavaMethod(std::string, const char* , std::string);
};


#endif //TEST_GRADLE_EXPERIMENTAL_PLUGIN_API_CALLER_H
