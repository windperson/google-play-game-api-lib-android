//
// Created by isak on 10/28/2016.
//

#include "GooglePlayGameJNI_Helper.h"

GooglePlayGameJNI_Helper *GooglePlayGameJNI_Helper::getInstance(JavaVM *vm, jint jni_version,
                                                                jobject googleApiClient) {

    if (JNI_Helper::singleton) {
        return (GooglePlayGameJNI_Helper *) JNI_Helper::singleton;
    }

    GooglePlayGameJNI_Helper *instance = new GooglePlayGameJNI_Helper(vm, jni_version);
    JNI_Helper::singleton = instance;
    JNIEnv *env = instance->getJNIEnv(NULL);
    instance->GoogleApiClient = env->NewGlobalRef(googleApiClient);

    return instance;
}

GooglePlayAchievement_Caller GooglePlayGameJNI_Helper::getGooglePlayAchievementCaller(
        std::string classFullName) {
    bool isDetached = false;
    JNIEnv *env = getJNIEnv(&isDetached);
    GooglePlayAchievement_Caller caller = GooglePlayAchievement_Caller(env, classFullName,
                                                                       GooglePlayGameJNI_Helper::singleton,
                                                                       this->getGoogleApiClient());
    caller.Detached = isDetached;

    return caller;
}

GooglePlayLeaderBoard_Caller GooglePlayGameJNI_Helper::getGooglePlayLeaderBoardCaller(
        std::string classFullName) {
    bool isDetached = false;
    JNIEnv *env = getJNIEnv(&isDetached);
    GooglePlayLeaderBoard_Caller caller = GooglePlayLeaderBoard_Caller(env, classFullName,
                                                                       GooglePlayGameJNI_Helper::singleton,
                                                                       this->getGoogleApiClient());
    caller.Detached = isDetached;

    return caller;
}


GooglePlayGameJNI_Helper::GooglePlayGameJNI_Helper(JavaVM *vm, jint jni_version) : JNI_Helper(vm,
                                                                                              jni_version) { }

jobject GooglePlayGameJNI_Helper::getGoogleApiClient() {
    return this->GoogleApiClient;
}

GooglePlayGameJNI_Helper::~GooglePlayGameJNI_Helper() {
    if (this->GoogleApiClient != NULL) {
        JNIEnv *env = getJNIEnv(NULL);
        env->DeleteGlobalRef(this->GoogleApiClient);
        this->GoogleApiClient = NULL;
    }

}
















